<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-learning
 * @author     Gregor Anzelj
 * @japanese translator Tomio Yanagisawa
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2009 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = '学習';
$string['mylearning'] = 'マイラーニング';

$string['learningsaved'] = '学習診断が保存されました。';
$string['learningsavefailed'] = '学習診断のアップデートに失敗しました。';

$string['multipleintelligences'] = '多重知能';
$string['multipleintelligencesdesc'] = '
<p>多重知能論は1983年、ハワード・ガードナー博士によって開発されました。IQテストに基づいた、それまでの知能に関する理論が、ほんの一部の限られた見方であることを示唆したものでした。そのかわりに、ガードナー博士は子どもや大人の中に存在する幅広い人間の可能性を説明し、7つの異なる知能を提示しています。8つめの知能も90年代中盤にガードナーによって定義され発表されました。これらの知能は以下のとおりです。

<ul>
<li><b>言語的。</b> 話し言葉や書き言葉の使う能力。</li>
<li><b>論理的数学的。</b> 帰納的、演繹的思考、推論能力、論理的能力、数字や抽象的パターン認識の活用能力。</li>
<li><b>視覚的空間的。</b>概念的な物体及び空間次元の視覚化能力。</li>
<li><b>身体・筋感覚的。</b> 身体に関する見識及び肉体的運動を制御する能力。</li>
<li><b>音楽リズム的。</b>リズム、音色、拍子などと共に音楽を修得する能力。</li>
<li><b>対外的。</b> 人と効果的にコミュニケーションして、人間関係を構成する能力。</li>
<li><b>内省的。</b> 各自の感情、動機、心の中の状態、自省思想を理解する能力。</li>
<li><b>自然派環境派。</b> 自然界や環境を識別する能力。</li>
</ul>
</p>
';
$string['learningstyles'] = '学習スタイル';
$string['learningstylesdesc'] = '
<p>学習スタイルは単純に学習方法の好みです。特に最も学習しやすい個別学習に対しての教育学的方法論を含んでいます。いろいろなタイプの学習スタイルのカテゴリーのでも最も一般的で広く使われているのが、初期の脳言語学的プログラミング（VAK）モデルとして拡がった以下のフレミングのVARKモデルです。
<ul>
<li><b>視覚的学習者。</b> 目で見る事（スライドや表やハンドアウト等の視覚的サポートツール等の絵で考える）を好みます。</li>
<li><b>音声的学習者。</b> （講義や、議論、音声テープ等）聞く事で最も学びます。</li>
<li><b>読み書き好き学習者。</b> 読書や書く事で最も学びます。</li>
<li><b>筋感覚的学習者</b> や <b>触感的学習者。</b> 経験を通じて学ぶ事を好みます。動いたり、何かに触ったり、（世の中の行動的解説、科学的プロエジェクト、実験等）何かをしたり。</li>
</ul>
</p>
';

$string['legend'] = '基本';
$string['dateadded'] = '追加: ';

// Multiple Intelligences
$string['intelligenceA'] = '言語的知能';
$string['intelligenceB'] = '論理数学的知能';
$string['intelligenceC'] = '視覚空間的知能';
$string['intelligenceD'] = '身体運動的知能';
$string['intelligenceE'] = '音楽的知能';
$string['intelligenceF'] = '対人的知能';
$string['intelligenceG'] = '内省的知能';
$string['intelligenceH'] = '自然的知能';
$string['true'] = 'True';
$string['false'] = 'False';

// Multiple Intelligences: Verbal-Linguistic intelligence
$string['multipleintelligences.A1'] = '自分の新しいアイデアを他の人に伝えるのは簡単です。';
$string['multipleintelligences.A2'] = '会話や講義、人の話を聞くことで多くを学びます。';
$string['multipleintelligences.A3'] = '公開演説や討論に参加することが楽しい。';
$string['multipleintelligences.A4'] = 'ノートを取ると覚えたり理解したりするのに役立つ。';
// Multiple Intelligences: Logical-Mathematical intelligence
$string['multipleintelligences.B1'] = 'ひとつひとつ指示を受ける事が役立ちます。';
$string['multipleintelligences.B2'] = '問題解決や論理的パズルが理解しやすい。';
$string['multipleintelligences.B3'] = '経験や事柄のパターンや関連性を簡単に見抜ける。';
$string['multipleintelligences.B4'] = '素早く暗算をすませられる。';
// Multiple Intelligences: Visual-Spatial intelligence
$string['multipleintelligences.C1'] = '地図や設計図を読むのが得意です。';
$string['multipleintelligences.C2'] = '図表やグラフ、グラフィクな整理図を使って覚える方が良い。';
$string['multipleintelligences.C3'] = 'アイデアを頭で視覚化できる。';
$string['multipleintelligences.C4'] = '物事や場面を頭で絵として覚える事ができる。';
// Multiple Intelligences: Bodily-Kinesthetic intelligence
$string['multipleintelligences.D1'] = '自分の手で物作りをすることが好きです。';
$string['multipleintelligences.D2'] = '自分だけで実行する事で最も学べる。';
$string['multipleintelligences.D3'] = 'バランス感覚に優れていて動き回ることが好き。';
$string['multipleintelligences.D4'] = '何もしないでいるのは忙しくしているより退屈。';
// Multiple Intelligences: Musical intelligence
$string['multipleintelligences.E1'] = '歌詞やメロディーを覚えるのは簡単です。';
$string['multipleintelligences.E2'] = '悲しい時、音楽を聴くことで元気づけます。';
$string['multipleintelligences.E3'] = '複雑な音楽作品の中で各楽器の音を聞き分けられる。';
$string['multipleintelligences.E4'] = '歌を歌ったり楽器を演奏したりすることが楽しい。';
// Multiple Intelligences: Interpersonal intelligence
$string['multipleintelligences.F1'] = '良く仲間や同僚の中ではリーダーとして活躍します。';
$string['multipleintelligences.F2'] = 'グループでのイベントや社会的イベントで楽しめる。';
$string['multipleintelligences.F3'] = '周囲の人の雰囲気や感情に敏感だ。';
$string['multipleintelligences.F4'] = '「チームプレーヤー」だし、他人と交流することで最も学べる。';
// Multiple Intelligences: Intrapersonal intelligence
$string['multipleintelligences.G1'] = '自分の行動や考えに責任をもちます。';
$string['multipleintelligences.G2'] = '学ぶ前になんで学ぶべきなのかを知る必要があります。';
$string['multipleintelligences.G3'] = '一人でいたり、自分の人生について考えたりするのが好き。';
$string['multipleintelligences.G4'] = '一人で作業してもグループで仕事をするのと同じ位生産的。';
// Multiple Intelligences: Naturalistic intelligence
$string['multipleintelligences.H1'] = '分類することは新たなデータに意味を持たせるのに役立ちます。';
$string['multipleintelligences.H2'] = '植木やペットの世話をするのが好きです。';
$string['multipleintelligences.H3'] = '月の満ち欠けや潮の満ち引き等の自然現象を見たり、その解説を聞いたりするのが好きだ。';
$string['multipleintelligences.H4'] = '特に生物学や植物学、動物学等の自然を学ぶのが好き。';


// Learning Styles
$string['learningtypeV'] = '視覚的タイプ';
$string['learningtypeA'] = '音声的タイプ';
$string['learningtypeK'] = '運動的タイプ';
$string['never'] = '決して当てはまらない';
$string['rarely'] = 'ほとんど当てはまらない';
$string['sometimes'] = '時々当てはまる';
$string['often'] = 'しばしば当てはまる';
$string['always'] = 'いつも当てはまる';

// Learning Styles: Visual type
$string['learningstyles.V01'] = '教室では先生と目を合わせていることができる。';
$string['learningstyles.V02'] = 'ノートをつける時や授業用資料を読んでいる時に（色鉛筆やクレヨンなど）色を活用する。';
$string['learningstyles.V03'] = '自分がしなくてはならない時、道案内の説明より地図を描く方が良い';
$string['learningstyles.V04'] = '地図や、図表、グラフ等をより良く理解する。';
$string['learningstyles.V05'] = '何かする時、ラジオがついていると邪魔。';
$string['learningstyles.V06'] = '解説を読んだり聞いたりするとき、たくさんノートをとる。';
$string['learningstyles.V07'] = '筆記テストをしている時、答えを学んだノートや本のページを容易にイメージできる。
';
$string['learningstyles.V08'] = 'ノートをとる方が覚えやすい。';
$string['learningstyles.V09'] = '例えば誰かの電話番号など何か暗記したい時、頭の中でイメージを作り出すと覚えやすい。';
$string['learningstyles.V10'] = '読んだり聞いている事を頭の中でイメージしやすい。';
$string['learningstyles.V11'] = '誰かが読んでくれるより一人で読書する方が良い。';
// Learning Styles: Auditory type
$string['learningstyles.A01'] = '他人と話している時の方が、簡単に理解しやすい。';
$string['learningstyles.A02'] = '文書の指示書より、口述のものの方が良い。';
$string['learningstyles.A03'] = '読んで理解するより、音声テープやオーディオCD等を聞く方が良い。';
$string['learningstyles.A04'] = '口述の質問より筆記テストの方が成績が悪い。';
$string['learningstyles.A05'] = '見慣れない物、事、環境などをイメージしにくい。';
$string['learningstyles.A06'] = 'ジョークを言うのが大好きで簡単に覚えておける。';
$string['learningstyles.A07'] = '先生と目を合わせていられないけど、説明を良く聞くことができる。';
$string['learningstyles.A08'] = '書き物をしている時、話しているのが好きだ。';
$string['learningstyles.A09'] = '読書している時、頭に浮かんだ言葉を聞いている。';
$string['learningstyles.A10'] = '人がどのように見えたか良く覚えられないけど、言葉は良く覚えている。';
$string['learningstyles.A11'] = '声を出して覚えると、暗記しやすい。';
// Learning Styles: Kinesthetic type
$string['learningstyles.K01'] = '肉体的に活動している時に良いアイデアが浮かぶ。';
$string['learningstyles.K02'] = '学ぶ時、テーブルに座っていたくなくて、異なる場所（床やベッド）の方を選ぶ。';
$string['learningstyles.K03'] = 'ノートをつけるけど、うまく構成できない。';
$string['learningstyles.K04'] = '長くじっと座っていられない。';
$string['learningstyles.K05'] = '自分の手を使って物事をすることが好き。';
$string['learningstyles.K06'] = '学んでいる時、休憩がたくさん必要。';
$string['learningstyles.K07'] = '話している時、身体も動いている。（ジェスチャーしてる。）';
$string['learningstyles.K08'] = '作り方を聞いているより、すぐに作ってしまいたい。';
$string['learningstyles.K09'] = '説明を聞いている時、良くいたずら書きを紙や作業台に書いている。';
$string['learningstyles.K10'] = '何を学んでいるときもひな形を作る事が好き。';
$string['learningstyles.K11'] = 'エッセイや要約を書く事より、プロジェクトになった活動をするのが好き。
';

?>
